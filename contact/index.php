<!DOCTYPE html>
<html lang="en">

    <head>
        <!-- Meta -->
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- SEO -->
        <title>Contact | Jorit Vásconez Gerlach</title>
        <meta name="description" content="This is my contact website, by Jorit Vásconez Gerlach. I am always happy to be contacted. So don't wait long and write to me.">
        <meta name="keywords" content="Jorit Vásconez Gerlach, Kontakt, Anschreiben, Vernetzen, Kontaktieren, Contact">
        <!-- Favicon -->
        <link rel="shortcut icon" href="https://jorit.vasconezgerlach.de/assets/icons/favicons/favicon.ico" type="image/x-icon" sizes="96x96">
        <link rel="icon" href="https://jorit.vasconezgerlach.de/assets/icons/favicons/favicon.png" type="image/png" sizes="144x144">
        <link rel="apple-touch-icon" href="https://jorit.vasconezgerlach.de/assets/icons/favicons/apple-touch-icon.png" type="image/png" sizes="144x144">
        <!-- Links -->
        <link rel="stylesheet" href="https://jorit.vasconezgerlach.de/assets/docs/css/main.css?">
        <link rel="stylesheet" href="page.css?v==<?php uniqid() ?>">

        <!-- hcaptcha -->
        <script src="https://js.hcaptcha.com/1/api.js" async defer></script>

        <!-- Scripts -->
        <script src="page.js?v=<?php uniqid() ?>"> </script>
    </head>

    <body>

        <header>
            <div class="logo">
                <a href="https://jorit.vasconezgerlach.de/"><img src="https://jorit.vasconezgerlach.de/assets/icons/logo.png" alt="Jorit Vásconez Gerlach Logo"></a>
            </div>
            <nav>
                <ul>
                    <a href="https://jorit.vasconezgerlach.de/portfolio"><li>Portfolio</li></a>
                    <li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</li>
                    <a href="https://jorit.vasconezgerlach.de/biography"><li>Biography</li></a>
                    <li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</li>
                    <a href="https://jorit.vasconezgerlach.de/contact"><li>Contact</li></a>
                </ul>
            </nav>
        </header>

        <main>
            <section id="landing">
                <h1>Tell Me</h1>
            </section>
            <section id="form">
                <form id="contactForm" action="javascript:">
                    <input type="text" name="name" id="formName" placeholder="Name" required>
                    <input type="email" name="email" id="formMail" placeholder="E-Mail" required>
                    <textarea name="message" id="formMessage" cols="30" rows="10" placeholder="Message" required></textarea>
                    <div class="chaptcha" style="display: none;">
                        <div class="h-captcha" data-sitekey="cb57e318-b451-4868-9b97-341e5251744e"></div>
                    </div>
                    <input type="submit" id="forrmSubmit" value="Send"></input>
                </form>
            </section>
        </main>

        <footer>
            Website made by Jorit
            <script> </script>
        </footer>
        
        <screenTooSmall>
            <p>Sorry...</p>
            <p>Your device screen is too small!</p>
        </screenTooSmall>

</html>