window.addEventListener('load', ()=>{

          const form = document.querySelector('form');
          const formChaptcha = form.querySelector('.chaptcha');

          form.onsubmit = ()=>{

                    verificateForFormSubmission();
                    
          }

          function verificateForFormSubmission() {

                    var formInputName = document.getElementById('formName').value;
                    var formInputMail = document.getElementById('formMail').value;
                    var formInputMessage = document.getElementById('formMessage').value;

                    formChaptcha.style.display = '';

                    form.querySelector('.h-captcha iframe').addEventListener('change', ()=>{
                              console.log("ready");
                    });

                    var waitForSome = setInterval(()=>{

                              console.log("Waiting for captcha...");

                              if(form.querySelector('.h-captcha iframe').getAttribute('data-hcaptcha-response') != "") {
                                        formChaptcha.style.display = 'none';
                                        sendContactMail(formInputName, formInputMail, formInputMessage);
                                        clearInterval(waitForSome);
                              }

                    }, 100);

          }
          
          function sendContactMail(name, mail, message) {

                    var hcaptcha = form.querySelector('.h-captcha iframe').getAttribute('data-hcaptcha-response');

                    // Set Data as JSON
                    const data = {
                              "name": name,
                              "mail": mail,
                              "message": message,
                              "hcaptcha": hcaptcha
                    };

                    // Create XHR Request
                    var xhr = new XMLHttpRequest();

                    // Setup XHR Request
                    var xhrURL = "https://jorit.vasconezgerlach.de/contact/server/";
                    xhr.open("POST", xhrURL, true);
                    xhr.setRequestHeader('Content-Type', 'application/json');
                    xhr.setRequestHeader("Accept", "application/json");

                    // On XHR Request Change
                    xhr.onreadystatechange = function () {
                              // On XHR Ready
                              if (xhr.readyState === 4) {
                                        // response
                                        console.log(xhr.responseText);
                                        var response = JSON.parse(xhr.responseText);
                                        if(response['success']) {
                                                  // mail send
                                                  contactForm.reset();
                                        } else {
                                                  // mail not send
                                                  verificateForFormSubmission();
                                        }
                              }
                    };
                    // Send XHR Request
                    xhr.send(JSON.stringify(data));
          }

});