<!DOCTYPE html>
<!-- comment section -->
<html lang="de">

    <head>
        <!-- Title -->
        <title>Portfolio | Jorit Vásconez Gerlach</title>
        <!-- Links -->
            <!-- Stylesheets -->
            <link rel="stylesheet" href="https://jorit.vasconezgerlach.de/assets/docs/css/main.css?v=000.02">
            <link rel="stylesheet" href="page.css?">
            <!-- Favicons -->
            <link rel="shortcut icon" href="https://jorit.vasconezgerlach.de/assets/icons/favicons/favicon.ico" type="image/x-icon" sizes="96x96">
            <link rel="icon" href="https://jorit.vasconezgerlach.de/assets/icons/favicons/favicon.png" type="image/png" sizes="144x144">
            <link rel="apple-touch-icon" href="https://jorit.vasconezgerlach.de/assets/icons/favicons/apple-touch-icon.png" type="image/png" sizes="144x144">
        <!-- Meta -->
            <!-- SEO texts -->
            <meta name="description" content="This is my portfolio website, by Jorit Vásconez Gerlach. Here I present my projects and achievements.">
            <meta name="keywords" content="Jorit Vásconez Gerlach, Projekte, Errungenschaften, Zeugnisse, Qualifikationen, Protfolio">
            <!-- Propertys -->
            <meta charset="UTF-8">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>

    <body>

        <header>
            <div class="logo">
                <a href="https://jorit.vasconezgerlach.de/"><img src="https://jorit.vasconezgerlach.de/assets/icons/logo.png" alt="Jorit Vásconez Gerlach Logo"></a>
            </div>
            <nav>
                <ul>
                    <a href="https://jorit.vasconezgerlach.de/portfolio"><li>Portfolio</li></a>
                    <li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</li>
                    <a href="https://jorit.vasconezgerlach.de/biography"><li>Biography</li></a>
                    <li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</li>
                    <a href="https://jorit.vasconezgerlach.de/contact"><li>Contact</li></a>
                </ul>
            </nav>
        </header>

        <main>
            <section id="landing">
                <h1>Portfolio</h1>
            </section>
            <section id="projects">
				<div class="card">
                    <div class="img">
                        <img src="https://jorit.vasconezgerlach.de/portfolio/projects/queens-landing.png" alt="">
                    </div>
                    <h3>Queens Teppichhaus</h3>
                    <p>carpet business</p>
                </div>
				<div class="card">
                    <div class="img">
                        <img src="https://jorit.vasconezgerlach.de/portfolio/projects/kv-bouw.png" alt="">
                    </div>
                    <h3>KV Bouw</h3>
                    <p>architecture office</p>
                </div>
				<div class="card">
                    <div class="img">
                        <img src="https://jorit.vasconezgerlach.de/portfolio/projects/mjp-landing.png" alt="">
                    </div>
                    <h3>MJ Partners</h3>
                    <p>civil engineer</p>
                </div>
				<div class="card">
                    <div class="img">
                        <img src="https://jorit.vasconezgerlach.de/portfolio/projects/aachen-app-landing.png" alt="">
                    </div>
                    <h3>Aachen App</h3>
                    <p>product website</p>
                </div>
				<div class="card">
                    <div class="img">
                        <img src="https://jorit.vasconezgerlach.de/portfolio/projects/jorit-landing.png" alt="">
                    </div>
                    <h3>Jorit Vásconez Gerlach</h3>
                    <p>my own website</p>
                </div>
				<div class="card">
                    <div class="img">
                        <img src="https://jorit.vasconezgerlach.de/portfolio/projects/slg-landing.png" alt="">
                    </div>
                    <h3>SLG Aachen</h3>
                    <p>school website</p>
                </div>
				<div class="card">
                    <div class="img">
                        <img src="https://jorit.vasconezgerlach.de/portfolio/projects/gpa-landing.png" alt="">
                    </div>
                    <h3>GP Architekturbüro</h3>
                    <p>architecture office</p>
                </div>
            </section>
        </main>

        <footer>
            Website made by Jorit
        </footer>

        <screenTooSmall>
            <p>Sorry...</p>
            <p>Your device screen is too small!</p>
        </screenTooSmall>

        <script src="pageload.js"></script>
    </body>
</html>