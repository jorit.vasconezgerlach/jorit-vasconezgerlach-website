<!DOCTYPE html>
<!-- comment section -->
<html lang="de">

    <head>
        <!-- Title -->
        <title>Biography | Jorit Vásconez Gerlach</title>
        <!-- Links -->
            <!-- Stylesheets -->
            <link rel="stylesheet" href="https://jorit.vasconezgerlach.de/assets/docs/css/main.css?">
            <link rel="stylesheet" href="page.css?v=<?php echo uniqid() ?>">
            <!-- Favicons -->
            <link rel="shortcut icon" href="https://jorit.vasconezgerlach.de/assets/icons/favicons/favicon.ico" type="image/x-icon" sizes="96x96">
            <link rel="icon" href="https://jorit.vasconezgerlach.de/assets/icons/favicons/favicon.png" type="image/png" sizes="144x144">
            <link rel="apple-touch-icon" href="https://jorit.vasconezgerlach.de/assets/icons/favicons/apple-touch-icon.png" type="image/png" sizes="144x144">
        <!-- Meta -->
            <!-- SEO texts -->
            <meta name="description" content="This is my biography website, by Jorit Vásconez Gerlach. Here I list my résumé and show who I am.">
            <meta name="keywords" content="Jorit Vásconez Gerlach, Leben, Geschichte, Leben, Biography">
            <!-- Propertys -->
            <meta charset="UTF-8">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>

    <body>

        <header>
            <div class="logo">
                <a href="https://jorit.vasconezgerlach.de/"><img src="https://jorit.vasconezgerlach.de/assets/icons/logo.png" alt="Jorit Vásconez Gerlach Logo"></a>
            </div>
            <nav>
                <ul>
                    <a href="https://jorit.vasconezgerlach.de/portfolio"><li>Portfolio</li></a>
                    <li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</li>
                    <a href="https://jorit.vasconezgerlach.de/biography"><li>Biography</li></a>
                    <li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</li>
                    <a href="https://jorit.vasconezgerlach.de/contact"><li>Contact</li></a>
                </ul>
            </nav>
        </header>

        <main>
            <section id="landing">
                <h1>Biography</h1>
            </section>
            <section id="biography">
                <div class="topic">
                    <h2>Swibble Co-Founder</h2>
                    <h3>2021 - Today</h3>
                    <p>My friend Fynn Frings and I firstly developed an released App together in 2021. It is called 'Aachen App' and should help physical shops selling their product online. With the release one local newspaper and to radio stations helped us to gain attention, this is why companies wrote us to make them their own apps. Since then we're serving App, Web and Backend development. One big plan is to develop a Swibble Service for other teams to organize internal stuff while sharing the most important information out of it directly to the customer.</p>
                </div>
                <div class="topic">
                    <h2>Building Myself</h2>
                    <h3>2019 - Today</h3>
                    <p>Showcasing me as a person, my projects and more is helping gaining attention and helps people inform about me.</p>
                </div>
                <div class="topic">
                    <h2>JRT Companies</h2>
                    <h3>2019 - Today</h3>
                    <p>Creating something that really could change the world should make it a better place is the only thing I want. With JRT I started publishing different tools with open source projects to simplify living. I want to build a decentralized company with a huge team to outsource humanity and finally live in an era where everyone focuses on living.</p>
                </div>
                <div class="topic">
                    <h2>Student at St. Leonhard Gymnasium, Aachen</h2>
                    <h3>2016 - 2022</h3>
                    <p>I was attending a really nice school St. Leonhard Gymnasium, Aachen Germany but was never happy with the system. I was curious about building businesses, learning how people work and to understand life. From early age on I created websites for the entire class with different information. Once I wrote a blog post to prepare everyone for a physics test and I still remember all the good feedback I got and the happy faces of kids happy with their grades. But there were so much more: Dropping a merch collection of a teacher; Building online shop for our school bakery; Writing a stage play where everyone has his role; Designing hoodies for a excursion and producing a short film about it; Catching people by making minimalist, good researched and presented presentations.</p>
                </div>
                <div class="topic">
                    <h2>Montessori Elementary School, Aachen</h2>
                    <h3>2012 - 2016</h3>
                    <p>The time in elementary school, was the most important for becoming who I am. In this time I also did different thing. Once I wanted to sum a number as often as I can with his own, starting from 1. I took these math papers with the squares on it. The numbers in every row fit into one of the squares, but this couldn't stop me. After weeks of writing an summing I finally had a roll of paper that was almost as long as the hallway of the school.</p>
                    <p>Another time it was very hot at school and many children asked for heat free, so I looked for a team in my class with which we started to prepare many leaflets that we then distributed during the next break. These leaflets were a call for a protest demanding that we get off today. I still remember exactly the moment when the break was over and I marched up the stairs with hundreds of students behind me.</p>
                </div>
            </section>
        </main>

        <footer>
            Website made by Jorit
        </footer>

        <screenTooSmall>
            <p>Sorry...</p>
            <p>Your device screen is too small!</p>
        </screenTooSmall>

    </body>

</html>