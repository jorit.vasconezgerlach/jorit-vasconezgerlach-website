<!DOCTYPE html>
<html lang="en">

    <head>
        <!-- Meta -->
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- SEO -->
        <title>Jorit Vásconez Gerlach</title>
        <meta name="description" content="Hello, my name is Jorit Vásconez Gerlach. I am a creative developer currently working in the freelancing field.">
        <meta name="keywords" content="Jorit Vásconez Gerlach, Aachen, Website, Information, Development, Creative, Design, Kreativ, Programmieren">
        <!-- Favicon -->
        <link rel="shortcut icon" href="https://jorit.vasconezgerlach.de/assets/icons/favicons/favicon.ico" type="image/x-icon" sizes="96x96">
        <link rel="icon" href="https://jorit.vasconezgerlach.de/assets/icons/favicons/favicon.png" type="image/png" sizes="144x144">
        <link rel="apple-touch-icon" href="https://jorit.vasconezgerlach.de/assets/icons/favicons/apple-touch-icon.png" type="image/png" sizes="144x144">
        <!-- Links -->
        <link rel="stylesheet" href="https://jorit.vasconezgerlach.de/assets/docs/css/main.css?">
        <link rel="stylesheet" href="page.css?">
        <!-- Scripts -->
        <script src="page.js"> </script>
    </head>

    <body>
        <header>
            <div class="logo">
                <img src="https://jorit.vasconezgerlach.de/assets/icons/logo.png" alt="">
            </div>
            <nav>
                <ul>
                    <a href="https://jorit.vasconezgerlach.de/portfolio"><li>Portfolio</li></a>
                    <li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</li>
                    <a href="https://jorit.vasconezgerlach.de/biography"><li>Biography</li></a>
                    <li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</li>
                    <a href="https://jorit.vasconezgerlach.de/contact"><li>Contact</li></a>
                </ul>
            </nav>
        </header>

        <main>
            <section id="landingFull">
                <div class="text">
                    <h1 style="opacity: 0;">Jorit Vásconez Gerlach</h1>
                    <h2 style="opacity: 0;">Creative Development</h2>
                </div>
                <div class="image">
                    <img src="https://jorit.vasconezgerlach.de/assets/imgs/jorit-portrait-photo-landingsection-compressed.png" alt="Jorit Vásconez Gerlach Portrait Photo">
                </div>
            </section>
            <section id="what">
                <div class="col two">
                    <div class="background"></div>
                    <h3>Development</h3>
                    <h2 class="typing">Websites, Apps, Backend</h2>
                </div>
                <div class="col one">
                    <h3>Creative</h3>
                    <h2 class="rotating"><span>Design</span><span>Marketing</span><span>Talking</span><span>Evolving</span></h2>
                </div>
            </section>
            <section id="how">
                <h2>How?</h2>
                <div class="use">
                    <h3>Preparing</h3>
                    <p>Before starting with the actual work, it's important for me to know what the client wants and expects.</p>
                    <p>In this step I talk with the client and occupied myself extensively with his idea. I prepair first designs to to be able to see better what the finished product looks like. First when the client thinks, that everything it's ready to go I'll start with the next step...</p>
                </div>
                <div>
                    <h3>Creating</h3>
                    <p>Now it's time to take everything that was prepared, sorting it and starting to work on the project.</p>
                    <p>I work with my hardware and software which I have collected over the years. These include from little unknow, but very useful applications up to huge programs to for example from Adobe.<br>In the whole process I like to stay in close contact to the customer for achieving better results.</p>
                </div>
                <div>
                    <h3>Finished</h3>
                    <p>A project is finished when the client is happy! And it's important for me to give him what i have.</p>
                    <p>My projects are 100% selfmade and I structure everything the way, that others can unterstand it and pass everything I have to the client. I'm sure, that I would love to keep working with you but no matter what, I am always available for questions about the project.</p>
            </section>
            <section id="why">
                <h2>Take A Look</h2>
                <div class="links">
                    <a href="https://jorit.vasconezgerlach.de/portfolio">My Portfolio</a>
                    <a href="https://jorit.vasconezgerlach.de/biography">My Biography</a>
                    <a href="https://jorit.vasconezgerlach.de/contact">Contact Me</a>
                </div>
            </section>
            <script>
                const text = ['Websites', 'Apps', 'Backend'];
                let count = 0;
                let index = 0;
                let letter = "";
                let currentText = "";

                typeWriting();
                function typeWriting() {
                    if(count == text.length) {
                        count = 0;
                    }
                    currentText = text[count];
                    letter = currentText.slice(0, index++);

                    if(letter=="") {
                        document.querySelector('.typing').style.color = "transparent";
                        document.querySelector('.typing').textContent = ".";
                    } else {
                        document.querySelector('.typing').style.color = "#FFFFFF";
                        document.querySelector('.typing').textContent = letter;
                    }

                    if(letter.length == currentText.length) {
                        count++;
                        index = 0;
                    }

                    setTimeout(typeWriting, 300);
                };

            </script>
        </main>

        <footer>
            <div class="vasconezgerlach">
                <h4>Vásconez Gerlach</h4>
                <a href="https://vasconezgerlach.de/info">Info</a>
            </div>
        </footer>

        <screenTooSmall>
            <p>Sorry...</p>
            <p>Your device screen is too small!</p>
        </screenTooSmall>

    </body>
</html>