waitForElement('landingFull', ()=>{
          const landing = document.getElementById('landingFull');
          const landingTitle = landing.querySelector('h1');
          const landingSubtitle = landing.querySelector('h2');
          slideAnimation(landingTitle);
          slideAnimation(landingSubtitle);
});

function waitForElement(elementId, callBack){
          window.setTimeout(()=>{
                    var element = document.getElementById(elementId);
                    if(element){
                              callBack(elementId, element);
                    }else{
                              waitForElement(elementId, callBack);
                    }
          }, 100);
}

window.addEventListener('load', ()=>{
          const whatIDoTextArray = ['Websites', 'Apps', 'Backend'];
          let count = 0;
          let index = 0;
          let letter = "";
          let currentText = "";

          typeWriting();

          function typeWriting() {
                    if(count == whatIDoTextArray.length) {
                        count = 0;
                    }
                    currentText = whatIDoTextArray[count];
                    letter = currentText.slice(0, index++);

                    if(letter=="") {
                        document.querySelector('.typing').style.color = "transparent";
                        document.querySelector('.typing').textContent = ".";
                    } else {
                        document.querySelector('.typing').style.color = "#FFFFFF";
                        document.querySelector('.typing').textContent = letter;
                    }

                    if(letter.length == currentText.length) {
                        count++;
                        index = 0;
                    }

                    setTimeout(typeWriting, 300);
          };
});

function slideAnimation(el) {
          // get content
          var text = el.textContent;
          var words = text.split(' ');
          // remove content
          el.innerHTML = '';
          // make visible
          el.style.opacity = 1;
          // apply styling
          el.style.display = 'flex';
          el.style.flexWrap = 'wrap';
          el.style.justifyContent = 'center';
          el.style.gap = '0.7rem';
          // init vars
          var letterPos = 1;
          // go trough all words
          words.forEach(word => {
                    // create word element
                    var newWordEl = document.createElement('div');
                    newWordEl.classList.add('word');
                    for(let i = 0; i < word.length; i++) {
                              // create letter element
                              var newLetterEl = document.createElement('span');
                              newLetterEl.classList.add('letter');
                              // get letter
                              newLetterEl.innerHTML = word[i];
                              newLetterEl.style.animationDelay = 0.05 * letterPos + 's';
                              newLetterEl.classList.toggle('animate');
                              newLetterEl.classList.toggle('animating');
                              newWordEl.appendChild(newLetterEl);
                              // new letter
                              letterPos++;
                    }
                    // append word element
                    el.appendChild(newWordEl);
          });
}