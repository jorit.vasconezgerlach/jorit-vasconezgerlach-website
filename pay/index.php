<!DOCTYPE html>
<html lang="en">

<head>

          <?php
                    $contentPath = "content.json";
                    $content = json_decode(file_get_contents($contentPath), true);
          ?>

          <meta charset="UTF-8">
          <meta http-equiv="X-UA-Compatible" content="IE=edge">
          <meta name="viewport" content="width=device-width, initial-scale=1.0">
          <title>Bank Details | Vásconez Gerlach</title>
          <!-- Link -->
          <link rel="stylesheet" href="../../assets/docs/css/main/get.css?v=<?php uniqid() ?>">
          <link rel="stylesheet" href="page.css?v=<?php uniqid() ?>">
          <!-- Favicons -->
          <link rel="shortcut icon" href="https://jorit.vasconezgerlach.de/assets/icons/favicons/favicon.ico"
                    type="image/x-icon" sizes="96x96">
          <link rel="icon" href="https://jorit.vasconezgerlach.de/assets/icons/favicons/favicon.png" type="image/png"
                    sizes="144x144">
          <link rel="apple-touch-icon"
                    href="https://jorit.vasconezgerlach.de/assets/icons/favicons/apple-touch-icon.png" type="image/png"
                    sizes="144x144">

          <script>
                    function toast(msg, data) {
                              if (document.getElementById('toastMessage')) {
                                        document.getElementById('toastMessage').remove();
                              }
                              var toast = Object.assign(document.createElement('div'), {
                                        id: 'toast',
                                        style: 'bottom: 20px; right: -350px; cursor: pointer; transition: 1s  cubic-bezier(0.25, 0.46, 0.45, 0.94); padding: 15px; width: 300px; max-width: 40%; position: fixed; z-index: 10000; background: #ca3216; color: white; box-shadow: rgba(0, 0, 0, 0.2) 0px 30px 90px;'
                              });
                              toast.id = 'toastMessage';
                              toast.innerText = msg;
                              toast.onclick = () => {
                                        data['callback']();
                                        document.getElementById('toastMessage').remove();
                              }
                              document.body.appendChild(toast);
                              setTimeout(() => {
                                        toast.style.right = '15px';
                                        setTimeout(() => {
                                                  toast.style.right = '-350px';
                                                  toast.style.transition = '1s  ease-in';
                                                  setTimeout(() => {
                                                            document.body.removeChild(toast);
                                                  }, 1000)
                                        }, 2000)
                              }, 1);
                    };
                    function copy(str, toastMessage) {
                              // Create new element
                              var el = document.createElement('textarea');
                              // Set value (string to be copied)
                              el.value = str;
                              // Set non-editable to avoid focus and move outside of view
                              el.setAttribute('readonly', '');
                              el.style = { position: 'absolute', left: '-9999px' };
                              document.body.appendChild(el);
                              // Select text inside element
                              el.select();
                              // Copy text to clipboard
                              document.execCommand('copy');
                              // Remove temporary element
                              document.body.removeChild(el);
                              // toast
                              toast(toastMessage);
                    }
          </script>

</head>

<body>

          <main>

                    <h2>Bank Details <span>Vásconez Gerlach</span></h2>
                    <div class="fields">
                              <label class="input copy" for="name">
                                        <span>Name</span>
                                        <div class="row">
                                                  <input id="name" type="text" value="<?php echo $content['name']?>"
                                                            readonly>
                                                  <div class="copy"
                                                            onclick="copy('<?php echo $content['name'] ?>', 'Name successfully copied')">
                                                            <svg version="1.1" id="Layer_1"
                                                                      xmlns="http://www.w3.org/2000/svg"
                                                                      xmlns:xlink="http://www.w3.org/1999/xlink" x="0px"
                                                                      y="0px" viewBox="0 0 507.9 507.9"
                                                                      style="enable-background:new 0 0 507.9 507.9;"
                                                                      xml:space="preserve">
                                                                      <g>
                                                                                <g>
                                                                                          <path d="M445.299,73.5h-74.8V14.1c0-7.8-6.3-14.1-14.1-14.1h-293.7c-7.8,0-14.1,6.3-14.1,14.1v390.8c0,7.8,6.3,14.1,14.1,14.1
				h59.3v74.8c0,7.8,6.3,14.1,14.1,14.1h213.3c3.7,0,7.3-1.5,10-4.1l95.8-95.8c2.6-2.6,4.1-6.2,4.1-10V87.6
				C459.399,79.8,453.099,73.5,445.299,73.5z M121.999,87.6v303.2h-45.2V28.2h265.5v45.3h-206.1
				C128.399,73.5,121.999,79.8,121.999,87.6z M363.599,459.8v-47.7h47.7L363.599,459.8z M431.199,383.9h-81.7
				c-7.8,0-14.1,6.3-14.1,14.1v81.7h-185.1v-378h280.9V383.9z" />
                                                                                </g>
                                                                      </g>
                                                                      <g>
                                                                                <g>
                                                                                          <path d="M385.199,151h-188.9c-7.8,0-14.1,6.3-14.1,14.1c0,7.8,6.3,14.1,14.1,14.1h188.9c7.8,0,14.1-6.3,14.1-14.1
				S392.999,151,385.199,151z" />
                                                                                </g>
                                                                      </g>
                                                                      <g>
                                                                                <g>
                                                                                          <path d="M385.199,229.9h-188.9c-7.8,0-14.1,6.3-14.1,14.1s6.3,14.1,14.1,14.1h188.9c7.8,0,14.1-6.3,14.1-14.1
				S392.999,229.9,385.199,229.9z" />
                                                                                </g>
                                                                      </g>
                                                                      <g>
                                                                                <g>
                                                                                          <path d="M385.199,308.7h-188.9c-7.8,0-14.1,6.3-14.1,14.1c0,7.8,6.3,14.1,14.1,14.1h188.9c7.8,0,14.1-6.3,14.1-14.1
				S392.999,308.7,385.199,308.7z" />
                                                                                </g>
                                                                      </g>
                                                                      <g>
                                                                                <g>
                                                                                          <path d="M283.199,387.5h-86.9c-7.8,0-14.1,6.3-14.1,14.1s6.3,14.1,14.1,14.1h86.9c7.8,0,14.1-6.3,14.1-14.1
				C297.299,393.8,290.999,387.5,283.199,387.5z" />
                                                                                </g>
                                                                      </g>
                                                            </svg>
                                                  </div>
                                        </div>
                              </label>
                              <label class="input copy" for="IBAN">
                                        <span>IBAN</span>
                                        <div class="row">
                                                  <input id="IBAN" type="text" value="<?php echo $content['iban'] ?>"
                                                            readonly>
                                                  <div class="copy"
                                                            onclick="copy('<?php echo $content['iban'] ?>', 'IBAN successfully copied')">
                                                            <svg version="1.1" id="Layer_1"
                                                                      xmlns="http://www.w3.org/2000/svg"
                                                                      xmlns:xlink="http://www.w3.org/1999/xlink" x="0px"
                                                                      y="0px" viewBox="0 0 507.9 507.9"
                                                                      style="enable-background:new 0 0 507.9 507.9;"
                                                                      xml:space="preserve">
                                                                      <g>
                                                                                <g>
                                                                                          <path d="M445.299,73.5h-74.8V14.1c0-7.8-6.3-14.1-14.1-14.1h-293.7c-7.8,0-14.1,6.3-14.1,14.1v390.8c0,7.8,6.3,14.1,14.1,14.1
				h59.3v74.8c0,7.8,6.3,14.1,14.1,14.1h213.3c3.7,0,7.3-1.5,10-4.1l95.8-95.8c2.6-2.6,4.1-6.2,4.1-10V87.6
				C459.399,79.8,453.099,73.5,445.299,73.5z M121.999,87.6v303.2h-45.2V28.2h265.5v45.3h-206.1
				C128.399,73.5,121.999,79.8,121.999,87.6z M363.599,459.8v-47.7h47.7L363.599,459.8z M431.199,383.9h-81.7
				c-7.8,0-14.1,6.3-14.1,14.1v81.7h-185.1v-378h280.9V383.9z" />
                                                                                </g>
                                                                      </g>
                                                                      <g>
                                                                                <g>
                                                                                          <path d="M385.199,151h-188.9c-7.8,0-14.1,6.3-14.1,14.1c0,7.8,6.3,14.1,14.1,14.1h188.9c7.8,0,14.1-6.3,14.1-14.1
				S392.999,151,385.199,151z" />
                                                                                </g>
                                                                      </g>
                                                                      <g>
                                                                                <g>
                                                                                          <path d="M385.199,229.9h-188.9c-7.8,0-14.1,6.3-14.1,14.1s6.3,14.1,14.1,14.1h188.9c7.8,0,14.1-6.3,14.1-14.1
				S392.999,229.9,385.199,229.9z" />
                                                                                </g>
                                                                      </g>
                                                                      <g>
                                                                                <g>
                                                                                          <path d="M385.199,308.7h-188.9c-7.8,0-14.1,6.3-14.1,14.1c0,7.8,6.3,14.1,14.1,14.1h188.9c7.8,0,14.1-6.3,14.1-14.1
				S392.999,308.7,385.199,308.7z" />
                                                                                </g>
                                                                      </g>
                                                                      <g>
                                                                                <g>
                                                                                          <path d="M283.199,387.5h-86.9c-7.8,0-14.1,6.3-14.1,14.1s6.3,14.1,14.1,14.1h86.9c7.8,0,14.1-6.3,14.1-14.1
				C297.299,393.8,290.999,387.5,283.199,387.5z" />
                                                                                </g>
                                                                      </g>
                                                            </svg>
                                                  </div>
                                        </div>
                              </label>
                              <label class="input copy" for="BIC">
                                        <span>BIC</span>
                                        <div class="row">
                                                  <input id="BIC" type="text" value="<?php echo $content['bic'] ?>" readonly>
                                                  <div class="copy"
                                                            onclick="copy('<?php echo $content['bic'] ?>', 'Bank name successfully copied')">
                                                            <svg version="1.1" id="Layer_1"
                                                                      xmlns="http://www.w3.org/2000/svg"
                                                                      xmlns:xlink="http://www.w3.org/1999/xlink" x="0px"
                                                                      y="0px" viewBox="0 0 507.9 507.9"
                                                                      style="enable-background:new 0 0 507.9 507.9;"
                                                                      xml:space="preserve">
                                                                      <g>
                                                                                <g>
                                                                                          <path d="M445.299,73.5h-74.8V14.1c0-7.8-6.3-14.1-14.1-14.1h-293.7c-7.8,0-14.1,6.3-14.1,14.1v390.8c0,7.8,6.3,14.1,14.1,14.1
				h59.3v74.8c0,7.8,6.3,14.1,14.1,14.1h213.3c3.7,0,7.3-1.5,10-4.1l95.8-95.8c2.6-2.6,4.1-6.2,4.1-10V87.6
				C459.399,79.8,453.099,73.5,445.299,73.5z M121.999,87.6v303.2h-45.2V28.2h265.5v45.3h-206.1
				C128.399,73.5,121.999,79.8,121.999,87.6z M363.599,459.8v-47.7h47.7L363.599,459.8z M431.199,383.9h-81.7
				c-7.8,0-14.1,6.3-14.1,14.1v81.7h-185.1v-378h280.9V383.9z" />
                                                                                </g>
                                                                      </g>
                                                                      <g>
                                                                                <g>
                                                                                          <path d="M385.199,151h-188.9c-7.8,0-14.1,6.3-14.1,14.1c0,7.8,6.3,14.1,14.1,14.1h188.9c7.8,0,14.1-6.3,14.1-14.1
				S392.999,151,385.199,151z" />
                                                                                </g>
                                                                      </g>
                                                                      <g>
                                                                                <g>
                                                                                          <path d="M385.199,229.9h-188.9c-7.8,0-14.1,6.3-14.1,14.1s6.3,14.1,14.1,14.1h188.9c7.8,0,14.1-6.3,14.1-14.1
				S392.999,229.9,385.199,229.9z" />
                                                                                </g>
                                                                      </g>
                                                                      <g>
                                                                                <g>
                                                                                          <path d="M385.199,308.7h-188.9c-7.8,0-14.1,6.3-14.1,14.1c0,7.8,6.3,14.1,14.1,14.1h188.9c7.8,0,14.1-6.3,14.1-14.1
				S392.999,308.7,385.199,308.7z" />
                                                                                </g>
                                                                      </g>
                                                                      <g>
                                                                                <g>
                                                                                          <path d="M283.199,387.5h-86.9c-7.8,0-14.1,6.3-14.1,14.1s6.3,14.1,14.1,14.1h86.9c7.8,0,14.1-6.3,14.1-14.1
				C297.299,393.8,290.999,387.5,283.199,387.5z" />
                                                                                </g>
                                                                      </g>
                                                            </svg>
                                                  </div>
                                        </div>
                              </label>
                    </div>

                    <section id="onlinePay">
                              <a href="https://paypal.me/joritsmoney">PayPal</a>
                    </section>

                    <section id="crypto" style="display: none;">
                              <div class="card bitcoin">
                                        <h4>Bitcoin</h4>
                                        <input type="text" value="bc1qa2udafykcd03rypwtddekc65pxkqa45t5mfezp" disabled>
                              </div>
                              <div class="card ether">
                                        <h4>Ethereum</h4>
                                        <input type="text" value="bc1qa2udafykcd03rypwtddekc65pxkqa45t5mfezp" disabled>
                                        <p class="warn">WARNING! Send tokens only through the Ethereum ERC20 network,
                                                  otherwise your money will be irretrievably lost.</p>
                              </div>
                    </section>

                    <section id="cardAdvertisments">

                              <h3>Advertisments</h3>

                              <?php
                                        foreach ($content['advertisments'] as $advertisment) {
                              ?>

                                        <div class="cardAdvertisment">
                                                  
                                                  <div class="close" onclick="smoothClose(this.parentNode)">
                                                            <svg width="24px" height="24px" viewBox="0 0 24 24"
                                                                      xmlns="http://www.w3.org/2000/svg">
                                                                      <g data-name="Layer 2">
                                                                                <g data-name="close">
                                                                                          <rect width="24" height="24"
                                                                                                    transform="rotate(180 12 12)"
                                                                                                    opacity="0" />
                                                                                          <path
                                                                                                    d="M13.41 12l4.3-4.29a1 1 0 1 0-1.42-1.42L12 10.59l-4.29-4.3a1 1 0 0 0-1.42 1.42l4.3 4.29-4.3 4.29a1 1 0 0 0 0 1.42 1 1 0 0 0 1.42 0l4.29-4.3 4.29 4.3a1 1 0 0 0 1.42 0 1 1 0 0 0 0-1.42z" />
                                                                                </g>
                                                                      </g>
                                                            </svg>
                                                  </div>

                                                  <h4><?php echo $advertisment['name'] ?></h4>
                                                  <div class="row">
                                                            <div class="info">
                                                                      <p><?php echo $advertisment['description'] ?></p>
                                                            </div>
                                                            <div
                                                                      style="display: block; background: #111; background: url('<?php echo $advertisment['imgPath'] ?>'); background-size: contain; min-width: 120px; min-height: 192px; border-radius: 15px;">
                                                            </div>
                                                  </div>
                                                  <a href="<?php echo $advertisment['url'] ?>"><?php echo $advertisment['buttonText'] ?></a>

                                        </div>

                              <?php
                                        }
                              ?>

                    </section>

                    <script>
                              function smoothClose(element) {
                                        element.style.transition = '0.4s ease-out';
                                        element.style.overflow = 'hidden';
                                        element.style.height = window.getComputedStyle(element, null).getPropertyValue('height');
                                        setTimeout(() => {
                                                  element.style.padding = window.getComputedStyle(element, null).getPropertyValue('padding-top') + window.getComputedStyle(element, null).getPropertyValue('padding-right') + ' 0px ' + window.getComputedStyle(element, null).getPropertyValue('padding-left');
                                                  element.style.height = '0px';
                                                  setTimeout(() => {
                                                            element.style.padding = '0px';
                                                  }, 250);
                                        }, 1);
                              }
                    </script>

          </main>
</body>

</html>